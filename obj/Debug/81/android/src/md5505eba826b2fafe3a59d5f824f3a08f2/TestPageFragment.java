package md5505eba826b2fafe3a59d5f824f3a08f2;


public class TestPageFragment
	extends android.support.v4.app.Fragment
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("DeskMatePro.Fragments.TestPageFragment, DeskMatePro", TestPageFragment.class, __md_methods);
	}


	public TestPageFragment ()
	{
		super ();
		if (getClass () == TestPageFragment.class)
			mono.android.TypeManager.Activate ("DeskMatePro.Fragments.TestPageFragment, DeskMatePro", "", this, new java.lang.Object[] {  });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
